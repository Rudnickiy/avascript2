// Теоретичні питання
// Які існують типи даних у Javascript?

// Мова JavaScript містить шість типів даних: Undefined (непроникний), Null (нульовий), Boolean (логічний), String (рядковий), Number (числовий) і Object (об'єктний)

// У чому різниця між == і ===?

// == використовується для порівняння двох змінних, але він ігнорує тип даних змінної, тоді як === використовується для порівняння двох змінних, але цей оператор також перевіряє тип даних і порівнює два значення.

// Що таке оператор?
// Оператор – це внутрішня функція JavaScript. При використанні того чи іншого оператора ми, по суті, просто запускаємо ту чи іншу вбудовану функцію, яка виконує певні дії і повертає результат.




function userVervication () {
    let userName = prompt('What is your name');

    while (userName === null || userName === "" || !isNaN(userName)) {
        userName = prompt ('What is your name', userName);
    }

    let userAge = prompt ('How old are you?');

    while (userAge === null || userAge === "" || isNaN(userAge)) {
        userAge = prompt('How old are you?' , userAge);
    }

    if (userAge < 18) {
        alert('You are not allowed to visit this website');
    } else if (userAge >=18 && userAge <= 22) {
        let allow = confirm('Are you sure you want to continue?');
        if (allow === true) {
            alert(`Welcome, ${userName} `);
        } else {
            alert('You are not allowed to visit this website');
        }
    } else {
        alert (`Welcome, ${userName}`);
    }
    console.log (`name: ${userName}, userAge: ${userAge}`);
}
userVervication ();



